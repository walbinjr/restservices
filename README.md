# Walbinjr Restservices

## Arquitetura

Para a solução do problema foram criadas 2 aplicações  (API), uma englobando cadastro de Campanhas e outra responsável pelo cadastro de Usuários e registro dos usuários nas campanhas ativas.

As aplicações foram criadas a partir do Spring Boot Initializr (https://start.spring.io).
Foram utilizados o modulos:
 - Web
 - JPA
 - Test
 - Actuator

Para desenvolvimento usei o IntelliJ IDEA
Para mapear e testar os endpoints usei o Postman `Walbinjr_Restservices.postman_collection.json` na raiz do projeto.

## API de Campanhas

Os controllers servem os endpoints da API e se comunicam com uma camada de serviço, que por sua vez chama o repositório que manipula o banco de dados.

Utilizei o Spring Actuator para servir um healthcheck da aplicação `/actuator/health`.

Esta API possui endpoints e validações de CRUD, bem como serve rotas de consultas específicas que são consumidas pela API de Usuários.

## API de Usuários (Sócio Torcedor)

Os controllers servem os endpoints da API e se comunicam com uma camada de serviço, que por sua vez chama o repositório que manipula o banco de dados.

Utilizei o Spring Actuator para servir um healthcheck da aplicação `/actuator/health`. Implementei um healthcheck para a API de Camapanhas.

Utilizei o próprio Spring Scheduler para checar se a API de campanhas está 'UP' e registrar os novos usuários nas campanhas ativas para o seu Time do Coração.

Esta API possui apenas endpoints e validações de CRUD.

## O que poderia ser diferente?

### Scheduler

Ao invés de usar o Scheduler que verifica uma flag no banco de dados para saber se é um novo usuário, poderia implementar um serviço de mensageria, usando RabbitMQ, por exemplo.

Com isso, ao cadastrar um novo usuário, salva na fila e um serviço apartado consome a fila de novos usuários, chamando as duas API para efetuar o registro de sócio torcedor.

### Docker

Não consegui tempo, mas eu gostaria de ter deixado o projeto para rodar com apenas um docker compose, subindo as 2 API's + Banco MySQL.

### Documentação

Também há a possibilidade de implementar um Swagger que documenta toda a API, além do Postman.

## Para rodar qualquer uma das aplicações

Exemplo rodando `restservice-campanha`:
```bash
# Para rodar
cd restservice-campanha
mvn clean spring-boot:run
```

```bash
# Para empacotar
cd restservice-campanha
./mvnw clean package
```

```bash
# Para rodar o jar
cd restservice-campanha/target
java -jar restservice-campanha/target/restservice-campanha-0.0.1-SNAPSHOT.jar
```