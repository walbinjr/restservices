package com.walbinjr.restservice.campanha.controller;

import com.walbinjr.restservice.campanha.exception.ResourceNotFoundException;
import com.walbinjr.restservice.campanha.model.Campaign;
import com.walbinjr.restservice.campanha.service.CampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/campaigns")
public class CampaignController {
    @Autowired
    private CampaignService campaignService;

    @GetMapping
    public List<Campaign> getAllCampaigns() {
        return campaignService.findAll();
    }

    @GetMapping("/active")
    public List<Campaign> getActiveCampaigns() {
        return campaignService.findAllActive();
    }

    @GetMapping("/active/{idTeam}")
    public List<Campaign> getActiveCampaignsByTeam(@PathVariable(value = "idTeam") Long idTeam) {
        return campaignService.findAllActiveByTeam(idTeam);
    }

    @GetMapping("/{id}")
    public Campaign getCampaignById(
            @PathVariable(value = "id") Long campaignId) throws ResourceNotFoundException {
        return campaignService.findById(campaignId)
                .orElseThrow(() -> new ResourceNotFoundException("Campaign not found on :: " + campaignId));
    }

    @PostMapping
    public Campaign createCampaign(@Valid @RequestBody Campaign campaign) {
        return campaignService.saveWithValidation(campaign);
    }

    @PutMapping("/{id}")
    public Campaign updateCampaign(
            @PathVariable(value = "id") Long campaignId,
            @Valid @RequestBody Campaign campaignDetails) throws ResourceNotFoundException {
        Campaign campaign = campaignService.findById(campaignId)
                .orElseThrow(() -> new ResourceNotFoundException("Campaign not found on :: " + campaignId));

        return campaignService.update(campaign, campaignDetails);
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteCampaign(
            @PathVariable(value = "id") Long campaignId) throws Exception {
        Campaign campaign = campaignService.findById(campaignId)
                .orElseThrow(() -> new ResourceNotFoundException("Campaign not found on :: " + campaignId));

        return campaignService.delete(campaign);
    }
}
