package com.walbinjr.restservice.campanha.repository;

import com.walbinjr.restservice.campanha.model.Campaign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CampaignRepository extends JpaRepository<Campaign, Long> {
    /**
     * Encontra campanhas inseridas no mesmo periodo de vigencia ordenadas pela data final.
     *
     * @param currentId id da campanha
     * @param startDate data de inicio da campanha
     * @param endDate   data de termino da campanha
     * @return lista de campanhas presentes no periodo informado.
     */
    @Query("SELECT c FROM Campaign c WHERE c.id <> :currentId AND c.startAt BETWEEN :startDate AND :endDate OR c.endAt BETWEEN :startDate AND :endDate ORDER BY c.endAt ASC")
    public List<Campaign> findByRange(@Param("currentId") Long currentId, @Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    /**
     * Encontra campanhas ativas.
     *
     * @return lista de campanhas ativas.
     */
    @Query("SELECT c FROM Campaign c WHERE CURRENT_DATE BETWEEN c.startAt AND c.endAt")
    public List<Campaign> findAllActive();

    /**
     * Encontra campanhas ativas pelo id do time.
     *
     * @param idTeam id do time
     * @return lista de campanhas ativas do time.
     */
    @Query("SELECT c FROM Campaign c WHERE CURRENT_DATE BETWEEN c.startAt AND c.endAt AND c.idTeam = :idTeam")
    public List<Campaign> findAllActiveByTeam(@Param("idTeam") Long idTeam);
}
