package com.walbinjr.restservice.campanha.service;

import com.walbinjr.restservice.campanha.model.Campaign;
import com.walbinjr.restservice.campanha.repository.CampaignRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CampaignService {
    @Autowired
    private CampaignRepository campaignRepository;

    public List<Campaign> findAll() {
        return campaignRepository.findAll();
    }

    public List<Campaign> findAllActive() {
        return campaignRepository.findAllActive();
    }

    public List<Campaign> findAllActiveByTeam(Long idTeam) {
        return campaignRepository.findAllActiveByTeam(idTeam);
    }

    public Optional<Campaign> findById(Long campaignId) {
        return campaignRepository.findById(campaignId);
    }

    public Campaign saveWithValidation(Campaign campaign) {
        this.updateEndDate(campaign);
        return campaignRepository.save(campaign);
    }

    public Campaign update(Campaign campaign, Campaign campaignDetails) {
        campaign.setName(campaignDetails.getName());
        campaign.setIdTeam(campaignDetails.getIdTeam());
        campaign.setStartAt(campaignDetails.getStartAt());
        campaign.setEndAt(campaignDetails.getEndAt());
        campaign.setUpdatedAt(new Date());
        return saveWithValidation(campaign);
    }

    public Map<String, Boolean> delete(Campaign campaign) {
        campaignRepository.delete(campaign);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    private void updateEndDate(Campaign campaign) {
        campaignRepository.findByRange(campaign.getId(), campaign.getStartAt(), campaign.getEndAt())
                .parallelStream()
                .forEach(otherCampaign -> {
                    if (otherCampaign.getId() != campaign.getId() && otherCampaign.getEndAt().equals(campaign.getEndAt())) {
                        otherCampaign.setEndAt(otherCampaign.getEndAt().plusDays(1));
                        otherCampaign.setUpdatedAt(new Date());
                        campaignRepository.save(otherCampaign);

                        this.updateEndDate(otherCampaign);
                    }
                });
    }
}
