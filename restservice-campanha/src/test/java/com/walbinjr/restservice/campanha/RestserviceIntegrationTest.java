package com.walbinjr.restservice.campanha;

import com.walbinjr.restservice.campanha.model.Campaign;
import com.walbinjr.restservice.campanha.service.CampaignService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.*;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class RestserviceIntegrationTest {
    private static final LocalDate START_DATE = LocalDate.parse("2019-01-01");
    private static final LocalDate END_DATE = LocalDate.parse("2019-10-10");
    private static final String ENDPOINT = "/api/v1/campaigns/";

    private Campaign campaign;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CampaignService mockService;

    @Before
    public void init() {
        campaign = new Campaign(1L, "Campanha 1", 1, START_DATE, END_DATE);
        when(mockService.findById(1L)).thenReturn(Optional.of(campaign));
    }

    @Test
    public void findByCampaignId_OK() throws Exception {
        mockMvc.perform(get(ENDPOINT + "1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Campanha 1")))
                .andExpect(jsonPath("$.idTeam", is(1)))
                .andExpect(jsonPath("$.endAt", is(END_DATE.toString())));

        verify(mockService, times(1)).findById(1L);

    }

    @Test
    public void findAllCampaigns_OK() throws Exception {
        List<Campaign> campaigns = Arrays.asList(
                new Campaign(1L, "Campanha 1", 1, START_DATE, END_DATE.plusDays(1)),
                new Campaign(2L, "Campanha 2", 2, START_DATE, END_DATE));

        when(mockService.findAll()).thenReturn(campaigns);

        mockMvc.perform(get(ENDPOINT))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Campanha 1")))
                .andExpect(jsonPath("$[0].idTeam", is(1)))
                .andExpect(jsonPath("$[0].endAt", is(END_DATE.plusDays(1).toString())))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("Campanha 2")))
                .andExpect(jsonPath("$[1].idTeam", is(2)))
                .andExpect(jsonPath("$[1].endAt", is(END_DATE.toString())));

        verify(mockService, times(1)).findAll();
    }

    @Test
    public void findByCampaignId_NotFound_404() throws Exception {
        mockMvc.perform(get(ENDPOINT + "5")).andExpect(status().isNotFound());
    }

    @Test
    public void saveCampaign_OK() throws Exception {
        when(mockService.saveWithValidation(any(Campaign.class))).thenReturn(campaign);

        String campaign = "{\"name\":\"Campanha 1\",\"idTeam\":1,\"startAt\":\"2019-01-01\",\"endAt\":\"2019-10-10\"}";

        mockMvc.perform(post(ENDPOINT)
                .content(campaign)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Campanha 1")))
                .andExpect(jsonPath("$.idTeam", is(1)))
                .andExpect(jsonPath("$.endAt", is(END_DATE.toString())));

        verify(mockService, times(1)).saveWithValidation(any(Campaign.class));
    }

    @Test
    public void updateCampaign_OK() throws Exception {
        campaign.setName("Campanha 1 (Atualizada)");
        when(mockService.update(any(Campaign.class), any(Campaign.class))).thenReturn(campaign);

        String campaign = "{\"name\":\"Campanha 1 (Atualizada)\",\"idTeam\":1,\"startAt\":\"2019-01-01\",\"endAt\":\"2019-10-10\"}";

        mockMvc.perform(put(ENDPOINT + "1")
                .content(campaign)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Campanha 1 (Atualizada)")))
                .andExpect(jsonPath("$.idTeam", is(1)))
                .andExpect(jsonPath("$.endAt", is(END_DATE.toString())));
    }

    @Test
    public void deleteCampaign_OK() throws Exception {
        Map<String, Boolean> responseMap = new HashMap<>();
        responseMap.put("deleted", Boolean.TRUE);
        when(mockService.delete(campaign)).thenReturn(responseMap);

        mockMvc.perform(delete(ENDPOINT + "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deleted", is(true)));

        verify(mockService, times(1)).delete(campaign);
    }
}
