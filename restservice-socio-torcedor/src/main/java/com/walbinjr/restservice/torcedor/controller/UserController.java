package com.walbinjr.restservice.torcedor.controller;

import com.walbinjr.restservice.torcedor.exception.ResourceNotFoundException;
import com.walbinjr.restservice.torcedor.model.User;
import com.walbinjr.restservice.torcedor.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public List<User> getAllUsers() {
        return userService.findAll();
    }

    @GetMapping("/team/{idTeam}")
    public List<User> getAllUsersByTeam(@PathVariable(value = "idTeam") Long idTeam) {
        return userService.findAllByTeam(idTeam);
    }

    @GetMapping("/{id}")
    public User getUserById(
            @PathVariable(value = "id") Long userId) throws ResourceNotFoundException {
        return userService.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
    }

    @PostMapping
    public User createUser(@Valid @RequestBody User user) {
        return userService.save(user);
    }

    @PutMapping("/{id}")
    public User updateUser(
            @PathVariable(value = "id") Long userId,
            @Valid @RequestBody User userDetails) throws ResourceNotFoundException {
        User user = userService.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));

        return userService.update(user, userDetails);
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteUser(
            @PathVariable(value = "id") Long userId) throws Exception {
        User user = userService.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));

        return userService.delete(user);
    }
}
