package com.walbinjr.restservice.torcedor.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
public class User {

    private long id;
    private String name;
    private String email;
    private LocalDate birthday;
    private long idTeam;
    private boolean newUser = true;
    private Date createdAt;
    private Date updatedAt;
    private List<Long> campaignsIds = new ArrayList<Long>();

    public User() {
    }

    public User(long id, String name, String email, LocalDate birthday, long idTeam) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.birthday = birthday;
        this.idTeam = idTeam;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    @NotBlank
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "id_team", nullable = false)
    @NotNull
    public long getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(long idTeam) {
        this.idTeam = idTeam;
    }

    @Column(name = "birthday", nullable = false)
    @NotNull
    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Column(name = "email", nullable = false, unique = true)
    @Email
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "new_user", nullable = false)
    @NotNull
    public boolean isNewUser() {
        return newUser;
    }

    public void setNewUser(boolean newUser) {
        this.newUser = newUser;
    }

    @ElementCollection
    @CollectionTable(
            name = "user_campaigns",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id")
    )
    @Column(name = "campaign_id")
    public List<Long> getCampaignsIds() {
        return campaignsIds;
    }

    public void setCampaignsIds(List<Long> campaignsIds) {
        this.campaignsIds = campaignsIds;
    }

    @Column(name = "created_at", nullable = false)
    @CreatedDate
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "updated_at", nullable = false)
    @LastModifiedDate
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", birthday=" + birthday +
                ", idTeam=" + idTeam +
                ", newUser=" + newUser +
                ", campaignsIds=" + campaignsIds +
                '}';
    }
}
