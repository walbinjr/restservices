package com.walbinjr.restservice.torcedor.repository;

import com.walbinjr.restservice.torcedor.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    /**
     * Encontra usuarios pelo id do time.
     *
     * @param idTeam id do time
     * @return lista de usuarios do time.
     */
    @Query("SELECT u FROM User u WHERE u.idTeam = :idTeam")
    public List<User> findAllByTeam(@Param("idTeam") Long idTeam);

    /**
     * Encontra usuarios novos
     *
     * @return lista de usuarios novos.
     */
    @Query("SELECT u FROM User u WHERE u.newUser = true")
    public List<User> findAllNewUsers();
}
