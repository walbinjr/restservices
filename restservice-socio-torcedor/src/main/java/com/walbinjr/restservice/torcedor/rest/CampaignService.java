package com.walbinjr.restservice.torcedor.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class CampaignService {
    Logger logger = LoggerFactory.getLogger(CampaignService.class);

    @Value("${restservice.endpoint.campaign.externalURL}")
    public String serviceURL;

    @Autowired
    private RestTemplate restTemplate;

    public List<Campaign> getActiveCampaignsByTeam(Long idTeam) throws Exception {
        Campaign[] campaigns = restTemplate.getForObject(
                serviceURL + "/api/v1/campaigns/active/" + idTeam, Campaign[].class);
        return Arrays.asList(campaigns != null ? campaigns : new Campaign[0]);
    }

    public boolean isUp() {
        try {
            ResponseEntity<String> response = restTemplate.getForEntity(serviceURL + "/actuator/health", String.class);
            response.getStatusCode();
            return response.getStatusCode().is2xxSuccessful();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }
}
