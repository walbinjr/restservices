package com.walbinjr.restservice.torcedor.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class CampaignServiceHealthIndicador implements HealthIndicator {
    @Autowired
    private CampaignService campaignService;

    @Override
    public Health health() {
        if (!isRunningCampaignService()) {
            return Health.down().withDetail("CampaignService", "Not Available").build();
        }
        return Health.up().withDetail("CampaignService", "Available").build();
    }

    private Boolean isRunningCampaignService() {
        return campaignService.isUp();
    }
}