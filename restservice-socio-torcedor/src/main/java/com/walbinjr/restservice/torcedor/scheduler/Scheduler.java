package com.walbinjr.restservice.torcedor.scheduler;

import com.walbinjr.restservice.torcedor.model.User;
import com.walbinjr.restservice.torcedor.rest.Campaign;
import com.walbinjr.restservice.torcedor.rest.CampaignService;
import com.walbinjr.restservice.torcedor.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class Scheduler {
    Logger logger = LoggerFactory.getLogger(Scheduler.class);

    @Autowired
    private CampaignService campaignService;

    @Autowired
    private UserService userService;

    @Scheduled(fixedRateString = "${scheduler.fixedRateValue}")
    public void updateNewUsersCampaigns() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        logger.info("Starting updateNewUsersCampaigns (" + strDate + ")...OK");
        boolean isUp = campaignService.isUp();
        logger.info("Checking if campaigns API is UP..." + isUp);
        if (isUp) {
            List<User> users = userService.findAllNewUsers();
            logger.info("Searching newUsers...Found " + users.size());
            for (User user : users) {
                try {
                    List<Campaign> campaigns = campaignService.getActiveCampaignsByTeam(user.getIdTeam());
                    logger.info("Retrieving Campaigns for user...Found " + campaigns.size());
                    List<Long> campaignIds = campaigns.stream().map(Campaign::getId).collect(Collectors.toList());
                    userService.updateCampaignIds(user, campaignIds);
                    logger.info("Updating user (" + user.getId() + ":" + user.getEmail() + ") with campaignIds..." + user.getCampaignsIds().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        logger.info("Exiting...OK");
    }
}