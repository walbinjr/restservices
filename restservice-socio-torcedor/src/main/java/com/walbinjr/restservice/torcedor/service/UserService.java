package com.walbinjr.restservice.torcedor.service;

import com.walbinjr.restservice.torcedor.model.User;
import com.walbinjr.restservice.torcedor.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public List<User> findAllByTeam(Long idTeam) {
        return userRepository.findAllByTeam(idTeam);
    }

    public List<User> findAllNewUsers() {
        return userRepository.findAllNewUsers();
    }

    public Optional<User> findById(Long userId) {
        return userRepository.findById(userId);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public User update(User user, User userDetails) {
        user.setName(userDetails.getName());
        user.setIdTeam(userDetails.getIdTeam());
        user.setBirthday(userDetails.getBirthday());
        user.setEmail(userDetails.getEmail());
        user.setUpdatedAt(new Date());
        user.setNewUser(false);
        return save(user);
    }

    public User updateCampaignIds(User user, List<Long> campaignIds) {
        user.setCampaignsIds(campaignIds);
        user.setNewUser(false);
        return save(user);
    }

    public Map<String, Boolean> delete(User user) {
        userRepository.delete(user);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
