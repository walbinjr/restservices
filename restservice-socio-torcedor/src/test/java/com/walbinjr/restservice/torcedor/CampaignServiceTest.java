package com.walbinjr.restservice.torcedor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.walbinjr.restservice.torcedor.model.User;
import com.walbinjr.restservice.torcedor.rest.Campaign;
import com.walbinjr.restservice.torcedor.rest.CampaignService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CampaignServiceTest {
    private static final Logger log = LoggerFactory.getLogger(CampaignServiceTest.class);
    private User user;
    private Campaign campaign;

    private MockRestServiceServer mockServer;
    private ObjectMapper mapper = new ObjectMapper();

    @Value("${restservice.endpoint.campaign.externalURL}")
    public String serviceURL;

    @Autowired
    private CampaignService campaignService;
    @Autowired
    private RestTemplate restTemplate;

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        campaign = new Campaign();
        campaign.setId(1);
        campaign.setName("Campanha 1");
        campaign.setIdTeam(1);
    }

    @Test
    public void findAllActiveCampaigns_OK() throws Exception {
        mockServer.expect(ExpectedCount.once(),
                requestTo(new URI(serviceURL + "/api/v1/campaigns/active/1")))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body("[{\"id\": 1,\"name\": \"Campanha 1\",\"idTeam\": 1}]")
                );

        List<Campaign> campaigns = campaignService.getActiveCampaignsByTeam(1l);
        mockServer.verify();
        Assert.assertEquals(campaign.toString(), campaigns.get(0).toString());
    }
}
