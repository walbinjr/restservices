package com.walbinjr.restservice.torcedor;

import com.walbinjr.restservice.torcedor.model.User;
import com.walbinjr.restservice.torcedor.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.*;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class RestserviceIntegrationTest {
    private static final LocalDate BIRTHDAY = LocalDate.parse("1990-01-01");
    private static final String ENDPOINT = "/api/v1/users/";

    private User user;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Before
    public void init() {
        user = new User(1L, "User 1", "user@gmail.com", BIRTHDAY, 1);
        when(userService.findById(1L)).thenReturn(Optional.of(user));
    }

    @Test
    public void findByUserId_OK() throws Exception {
        mockMvc.perform(get(ENDPOINT + "1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("User 1")))
                .andExpect(jsonPath("$.birthday", is(BIRTHDAY.toString())))
                .andExpect(jsonPath("$.idTeam", is(1)));

        verify(userService, times(1)).findById(1L);

    }

    @Test
    public void findAllUsers_OK() throws Exception {
        List<User> users = Arrays.asList(
                new User(1L, "User 1", "user@gmail.com", BIRTHDAY, 1),
                new User(2L, "User 2", "seconduser@gmail.com", BIRTHDAY, 2));

        when(userService.findAll()).thenReturn(users);

        mockMvc.perform(get(ENDPOINT))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("User 1")))
                .andExpect(jsonPath("$[0].email", is("user@gmail.com")))
                .andExpect(jsonPath("$[0].idTeam", is(1)))
                .andExpect(jsonPath("$[0].birthday", is(BIRTHDAY.toString())))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("User 2")))
                .andExpect(jsonPath("$[1].email", is("seconduser@gmail.com")))
                .andExpect(jsonPath("$[1].idTeam", is(2)))
                .andExpect(jsonPath("$[1].birthday", is(BIRTHDAY.toString())));

        verify(userService, times(1)).findAll();
    }

    @Test
    public void findAllUsersByTeam_OK() throws Exception {
        List<User> users = Arrays.asList(
                new User(1L, "User 1", "user@gmail.com", BIRTHDAY, 1),
                new User(2L, "User 2", "seconduser@gmail.com", BIRTHDAY, 1));

        when(userService.findAllByTeam(1l)).thenReturn(users);

        mockMvc.perform(get(ENDPOINT + "/team/1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("User 1")))
                .andExpect(jsonPath("$[0].email", is("user@gmail.com")))
                .andExpect(jsonPath("$[0].idTeam", is(1)))
                .andExpect(jsonPath("$[0].birthday", is(BIRTHDAY.toString())))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("User 2")))
                .andExpect(jsonPath("$[1].email", is("seconduser@gmail.com")))
                .andExpect(jsonPath("$[1].idTeam", is(1)))
                .andExpect(jsonPath("$[1].birthday", is(BIRTHDAY.toString())));

        verify(userService, times(1)).findAllByTeam(1l);
    }

    @Test
    public void findAllNewUsers_OK() throws Exception {
        List<User> users = Arrays.asList(
                new User(1L, "User 1", "user@gmail.com", BIRTHDAY, 1),
                new User(2L, "User 2", "seconduser@gmail.com", BIRTHDAY, 2));

        when(userService.findAllNewUsers()).thenReturn(users);
        userService.findAllNewUsers();
        verify(userService, times(1)).findAllNewUsers();
    }

    @Test
    public void findByUserId_NotFound_404() throws Exception {
        mockMvc.perform(get(ENDPOINT + "5")).andExpect(status().isNotFound());
    }

    @Test
    public void saveUser_OK() throws Exception {
        when(userService.save(any(User.class))).thenReturn(user);

        String user = "{\"name\":\"User 1\",\"email\":\"user@gmail.com\",\"birthday\":\"1990-01-01\",\"idTeam\":1}";

        mockMvc.perform(post(ENDPOINT)
                .content(user)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("User 1")))
                .andExpect(jsonPath("$.email", is("user@gmail.com")))
                .andExpect(jsonPath("$.idTeam", is(1)))
                .andExpect(jsonPath("$.birthday", is(BIRTHDAY.toString())));

        verify(userService, times(1)).save(any(User.class));
    }

    @Test
    public void updateUser_OK() throws Exception {
        user.setName("User 1 (Atualizado)");
        when(userService.update(any(User.class), any(User.class))).thenReturn(user);

        String user = "{\"name\":\"User 1 (Atualizado)\",\"email\":\"user@gmail.com\",\"birthday\":\"1990-01-01\",\"idTeam\":1}";

        mockMvc.perform(put(ENDPOINT + "1")
                .content(user)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("User 1 (Atualizado)")))
                .andExpect(jsonPath("$.email", is("user@gmail.com")))
                .andExpect(jsonPath("$.idTeam", is(1)))
                .andExpect(jsonPath("$.birthday", is(BIRTHDAY.toString())));
    }

    @Test
    public void deleteUser_OK() throws Exception {
        Map<String, Boolean> responseMap = new HashMap<>();
        responseMap.put("deleted", Boolean.TRUE);
        when(userService.delete(user)).thenReturn(responseMap);

        mockMvc.perform(delete(ENDPOINT + "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deleted", is(true)));

        verify(userService, times(1)).delete(any(User.class));
    }


    @Test
    public void updateUserCampaignIds_OK() throws Exception {
        List<Long> campaignIds = new ArrayList<>();
        campaignIds.add(1l);
        campaignIds.add(2l);
        user.setCampaignsIds(campaignIds);

        when(userService.save(any(User.class))).thenReturn(user);
        userService.updateCampaignIds(user, campaignIds);
        verify(userService, times(1)).updateCampaignIds(any(User.class), anyList());
    }
}
